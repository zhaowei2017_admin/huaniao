/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : huaniao

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2018-10-06 22:41:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL auto_increment,
  `uname` varchar(256) default NULL,
  `upassword` varchar(256) default NULL,
  `rolename` varchar(256) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员';

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'root', 'root', '超级管理员');

-- ----------------------------
-- Table structure for `coupon`
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) default NULL,
  `couponValue` decimal(10,2) default NULL,
  `couponStatus` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('1', '3', '8.00', '1');
INSERT INTO `coupon` VALUES ('2', '1', '8.00', '1');

-- ----------------------------
-- Table structure for `goods`
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(11) NOT NULL auto_increment COMMENT 'id',
  `goodsname` varchar(256) default NULL COMMENT '商品名称',
  `typeid` int(11) default NULL COMMENT '商品类别id',
  `createTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) default NULL COMMENT '商品状态:\r\n            0：未审核\r\n            1：通过\r\n            2:    驳回',
  `userid` int(11) default NULL COMMENT '创建人id',
  `price` decimal(10,2) default NULL COMMENT '价格，保留2位小数',
  `inventory` int(11) default NULL COMMENT '库存',
  `details` text,
  `img1` varchar(256) default NULL,
  `img2` varchar(256) default NULL,
  `img3` varchar(256) default NULL,
  `sellNum` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('8', '虎皮鹦鹉活体宠物鸟', '4', '2018-10-05 09:57:58', '1', null, '199.88', '99', '<div>\n	<br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t15139/175/2373393768/447526/e73ae5e0/5a9800abNd96551b8.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t18151/219/580550439/417364/be89e953/5a9800abNf8b5a269.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t15037/76/2395610734/359403/a2fa0f69/5a9800acN5a30bc01.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t17233/28/559605799/430082/324e19d7/5a9800abNfda855b0.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t18919/298/598576063/361252/46e9d33d/5a9800acN20d8ff45.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t15205/208/2362480946/438807/9275e8ae/5a9800acN40fec23b.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t19603/350/577489694/341974/67d9f24/5a9800aeNc4461a65.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t18847/183/599128025/330438/5b3263c0/5a9800b0N936ed2fb.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t18112/31/613713399/363075/426a9842/5a9800b0Ndf9fe49f.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t18538/259/616675274/251660/bf8597a1/5a9800aeN99d75c91.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t16918/293/625496754/366043/9e3e6bb6/5a9800b0Nc4c37d35.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t15370/251/2389272361/329615/a3e6b376/5a9800afNa12d60cf.jpg\" /><br />\n</div>', '/uploadPic/d61c8b35-af1d-43a9-8c21-2ca3dbb4a259.jpg', '/uploadPic/b9cffe7f-b890-4511-b620-9085561447c8.jpg', '/uploadPic/fc339245-8135-4315-9f9b-593880af5ecd.jpg', '80');
INSERT INTO `goods` VALUES ('9', 'DEERA招财鱼饲料', '7', '2018-10-05 11:28:19', '1', null, '50.50', '10001', '<img src=\"https://img10.360buyimg.com/imgzone/jfs/t24220/101/1428804054/528314/dfaf5b72/5b5ee976N6c8f0024.jpg\" />', '/uploadPic/06066ae4-e467-483c-8197-f47c5ba8a048.jpg', null, null, '300');
INSERT INTO `goods` VALUES ('10', '家养繁殖八哥鸟活体', '5', '2018-10-05 10:06:38', '1', null, '1200.00', '30', '<br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t25363/120/1000159707/146370/eb0fd880/5b853a2dN73e09355.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t24499/91/2534305880/264771/7822da3d/5b853a2dNe99108f8.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t22720/66/2537991774/159496/9b87e2bb/5b853a2dN741902fc.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t24001/135/2565297541/152106/cdef634f/5b853a2dN84fa3fc5.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t27424/163/114466190/165525/ca00b639/5b853a2dN4c143ae2.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t24244/85/2583084148/196929/cffbc7be/5b853a2dNa679445b.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t25249/295/1029905786/272111/135b6b73/5b853a2eN55dcaafe.jpg\" /><span style=\"color:#666666;font-family:Arial, &quot;font-size:16px;font-weight:700;background-color:#FFFFFF;\">家养繁殖八哥鸟活体</span>', '/uploadPic/bed753d0-696e-4ed4-93cc-bf05515a7792.jpg', '/uploadPic/2f2f6f43-c5bd-4452-ba10-dee3e25646f8.jpg', '/uploadPic/5e3505e3-013c-42bc-8c42-66ffc78d600b.jpg', '100');
INSERT INTO `goods` VALUES ('11', '贵货多肉', '9', '2018-10-05 11:31:02', '1', null, '12.00', '1111', '<div id=\"J-detail-pop-tpl-top-new\" style=\"margin:0px;padding:0px;color:#666666;font-family:tahoma, arial, &quot;background-color:#FFFFFF;\">\n	<p>\n		<a href=\"https://sale.jd.com/act/pSRsmIgqCGh0J.html\" target=\"_self\"><img src=\"https://img30.360buyimg.com/popWaterMark/jfs/t26077/266/1398647229/261832/ad9579c9/5bb0a645Nde86b3a2.jpg\" alt=\"关联版式990300.jpg\" /></a>\n	</p>\n</div>\n<div class=\"detail-content clearfix\" style=\"margin:10px 0px;padding:0px;background:#F7F7F7;color:#666666;font-family:tahoma, arial, &quot;\">\n	<div class=\"detail-content-wrap\" style=\"margin:0px;padding:0px;background-color:#FFFFFF;\">\n		<div class=\"detail-content-item\" style=\"margin:0px;padding:0px;\">\n			<div id=\"activity_header\" style=\"margin:0px;padding:0px;\">\n			</div>\n			<div id=\"J-detail-content\" style=\"margin:0px;padding:0px;\">\n				<p>\n					<img alt=\"\" id=\"2327dca22e994293a25d03b5b346fefe\" class=\"\" src=\"https://img13.360buyimg.com/imgzone/jfs/t25780/42/880415961/186914/8bbf55e5/5b7eaebdN2fafc865.jpg\" />\n				</p>\n				<p style=\"color:#000000;font-family:Arial, Verdana, sans-serif;\">\n					<img alt=\"\" id=\"084628e1676f4f02a351526b2c0e635f\" class=\"\" src=\"https://img10.360buyimg.com/imgzone/jfs/t25000/112/834118662/170919/b05c0e74/5b7eaebeNd5c99b1c.jpg\" /><br />\n<img alt=\"\" id=\"57d86dde28c34422b0463da611709a26\" class=\"\" src=\"https://img10.360buyimg.com/imgzone/jfs/t23632/314/2440517402/59912/8b201111/5b7eaebfN0bbb2d99.jpg\" />\n				</p>\n				<div style=\"margin:0px;padding:0px;\">\n					<br />\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n<br />', '/uploadPic/728bd8f1-a1bb-4ccb-9716-45343c4e3278.jpg', '/uploadPic/17a2c0ca-72a5-4077-8edf-dc5b1fa35131.jpg', '/uploadPic/852ed9cb-357c-4ae8-a2dc-c1696872ed0a.jpg', '500');
INSERT INTO `goods` VALUES ('12', '薄荷文竹仙人球掌', '9', '2018-10-05 11:32:40', '1', null, '12.00', '1000', '<img src=\"https://img10.360buyimg.com/imgzone/jfs/t5587/352/8919527256/180190/8e3542b1/598147e0N91616916.jpg\" />', '/uploadPic/9854e49d-0033-4d0b-9e14-e977f22ecfe5.jpg', null, null, '20');
INSERT INTO `goods` VALUES ('13', '圆形玻璃金鱼缸', '10', '2018-10-05 13:53:18', '1', null, '119.00', '111', '<ul id=\"parameter-brand\" class=\"p-parameter-list\" style=\"color:#666666;font-family:tahoma, arial, &quot;background-color:#FFFFFF;\">\n	<li>\n		<img id=\"1\" alt=\"\" class=\"\" src=\"https://img12.360buyimg.com/imgzone/jfs/t23548/195/2523550824/127449/9feff173/5b84ac16N7553bdd0.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t22210/6/1291504765/63269/7df6c4d3/5b24c5a0N5fc4f707.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img12.360buyimg.com/imgzone/jfs/t26383/237/100650011/101812/7b503b8e/5b84ac17N722b2e2d.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img11.360buyimg.com/imgzone/jfs/t23470/113/2552049260/106560/354f9454/5b84ac17N12af9dad.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img11.360buyimg.com/imgzone/jfs/t24349/307/2473552913/90935/f30f46bb/5b84ac17N1ae1ec59.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/imgzone/jfs/t27244/224/97820049/181908/36287aed/5b84ac18N400bd6ad.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img20.360buyimg.com/imgzone/jfs/t26908/240/95727066/146367/36818cfa/5b84ac1aNdb48eb3a.jpg\" /><br />\n<img id=\"1\" alt=\"\" class=\"\" src=\"https://img10.360buyimg.com/imgzone/jfs/t24598/336/998682179/106024/bff97cdf/5b84ac1aN5046f889.jpg\" /> \n	</li>\n</ul>\n<p>\n	<span><br />\n</span> \n</p>\n<p>\n	<br />\n</p>', '/uploadPic/918a27a2-4173-4cfb-b25f-6872dfe01129.jpg', null, null, '900');
INSERT INTO `goods` VALUES ('14', '君子兰盆栽花苗', '12', '2018-10-05 13:51:42', '1', null, '100.00', '100', '<img alt=\"\" class=\"\" src=\"https://img13.360buyimg.com/popWaterMark/jfs/t17704/34/2085472462/118057/ff6bf2fc/5ae33b5eN3caaebfa.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img10.360buyimg.com/popWaterMark/jfs/t17974/279/2090529455/185735/d97acde2/5ae33b5eN3487268b.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img13.360buyimg.com/popWaterMark/jfs/t19591/60/2095030424/212800/5115e4e4/5ae33b5eNadb8a2f7.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img10.360buyimg.com/popWaterMark/jfs/t19918/276/62251386/182936/1f3bd772/5ae33b5eN08881b72.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img10.360buyimg.com/popWaterMark/jfs/t18616/264/2102045077/203657/32c8f087/5ae33b5bN282e8145.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img12.360buyimg.com/popWaterMark/jfs/t18955/166/2103448830/225361/d8a04bdc/5ae33b5eN3d0ec340.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img11.360buyimg.com/popWaterMark/jfs/t18190/231/2059243803/277286/6a4aa11a/5ae33b5eN1a785321.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img11.360buyimg.com/popWaterMark/jfs/t18601/311/2057258024/220948/d4fc3d0f/5ae33b5bN0c8bfe1c.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img13.360buyimg.com/popWaterMark/jfs/t20113/275/70361180/137510/7dbd7ec8/5ae33b5eNc3b686c3.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img11.360buyimg.com/popWaterMark/jfs/t17299/289/2049435298/215137/9e829bdf/5ae33b5bN213948a3.jpg\" /><br />\n<img alt=\"\" class=\"\" src=\"https://img30.360buyimg.com/popWaterMark/jfs/t19048/194/2045621861/85333/b2a69e55/5ae33b5eNbef7b13f.jpg\" />', '/uploadPic/827363d2-9bdd-4027-9fbb-02592686d791.jpg', null, null, '300');
INSERT INTO `goods` VALUES ('15', '易萌宠物活体鸟观赏虎皮鹦鹉活体手养说话小鸟', '4', '2018-10-05 15:44:50', '1', null, '111.00', '100', '<img src=\"https://img10.360buyimg.com/imgzone/jfs/t14872/188/1904296869/164321/2e315d61/5a601addNf40914a5.jpg\" />', '/uploadPic/2e559a62-d077-473d-9cda-205e9711bd72.jpg', null, null, null);
INSERT INTO `goods` VALUES ('16', '灰珍珠玄凤', '4', '2018-10-05 15:44:59', '1', null, '111.00', '99', '<img src=\"https://img10.360buyimg.com/imgzone/jfs/t11914/201/1692596451/45167/3e685d4/5a06af42N7f0be55b.jpg\" />', '/uploadPic/31ffcaad-039d-4808-b38e-1a1665666fef.jpg', null, null, null);
INSERT INTO `goods` VALUES ('17', '白色玄风鹦鹉两只（一公一母）', '4', '2018-10-05 13:59:11', '1', null, '1230.00', '10', '<p>\n	<img src=\"https://img30.360buyimg.com/popWaterMark/jfs/t19213/107/625991113/482640/d15997ed/5a9a4d95Nae2e2d12.png\" />\n</p>\n<p>\n	<ul id=\"parameter-brand\" class=\"p-parameter-list\" style=\"color:#666666;font-family:tahoma, arial, &quot;background-color:#FFFFFF;\">\n		<li>\n			品牌：&nbsp;<a href=\"https://list.jd.com/list.html?cat=6994,14374,14377&amp;ev=exbrand_345272\" target=\"_blank\">double yellow</a>\n		</li>\n	</ul>\n	<ul class=\"parameter2 p-parameter-list\" style=\"color:#666666;font-family:tahoma, arial, &quot;background-color:#FFFFFF;\">\n		<li>\n			商品名称：虎皮鹦鹉活体玄风鹦鹉活体牡丹鹦鹉宠物鸟活体珍珠鸟文鸟活体说话鸟手养鸟幼鸟观赏鸟 白色玄风鹦鹉两只（一公一母）\n		</li>\n		<li>\n			商品编号：27111501963\n		</li>\n		<li>\n			店铺：&nbsp;<a href=\"https://mall.jd.com/index-709621.html\" target=\"_blank\">稻盛宠物用品专营店</a>\n		</li>\n		<li>\n			商品毛重：100.00g\n		</li>\n		<li>\n			是否有视频：是\n		</li>\n		<li>\n			宠物年龄：幼宠\n		</li>\n		<li>\n			国产/进口：国产\n		</li>\n		<li>\n			宠物性别：公\n		</li>\n		<li>\n			品类：鸟类\n		</li>\n	</ul>\n</p>', '/uploadPic/a29c992f-b4b0-4461-a11f-56998eda5606.jpg', null, null, null);

-- ----------------------------
-- Table structure for `goodstype`
-- ----------------------------
DROP TABLE IF EXISTS `goodstype`;
CREATE TABLE `goodstype` (
  `id` int(11) NOT NULL auto_increment COMMENT '主键id',
  `typename` varchar(256) default NULL COMMENT '类别名称',
  `typecode` varchar(256) default NULL COMMENT '类别代码',
  `parentid` int(11) default NULL COMMENT '父类别id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品类别表';

-- ----------------------------
-- Records of goodstype
-- ----------------------------
INSERT INTO `goodstype` VALUES ('3', '鸟类用品', 'niaolei', '0');
INSERT INTO `goodstype` VALUES ('4', '鹦鹉', 'yingwu', '3');
INSERT INTO `goodstype` VALUES ('5', '八哥', 'bage', '3');
INSERT INTO `goodstype` VALUES ('6', '孔雀', 'kongque', '3');
INSERT INTO `goodstype` VALUES ('7', '鱼饲料', '', '13');
INSERT INTO `goodstype` VALUES ('8', '花卉盆景', '', '0');
INSERT INTO `goodstype` VALUES ('9', '多肉植物', '', '8');
INSERT INTO `goodstype` VALUES ('10', '鱼缸', '', '13');
INSERT INTO `goodstype` VALUES ('11', '兰花', '', '0');
INSERT INTO `goodstype` VALUES ('12', '君子兰', '', '11');
INSERT INTO `goodstype` VALUES ('13', '水族用品', '', '0');
INSERT INTO `goodstype` VALUES ('14', '绿化苗木', '', '0');
INSERT INTO `goodstype` VALUES ('15', '草坪草种', '', '14');
INSERT INTO `goodstype` VALUES ('16', '庭院植物', '', '0');
INSERT INTO `goodstype` VALUES ('17', '葡萄树', '', '16');
INSERT INTO `goodstype` VALUES ('18', '狗狗用品', '', '0');
INSERT INTO `goodstype` VALUES ('19', '狗粮', '', '18');
INSERT INTO `goodstype` VALUES ('20', '猫咪用品', '', '0');
INSERT INTO `goodstype` VALUES ('21', '猫粮', '', '20');
INSERT INTO `goodstype` VALUES ('22', '兔子用品', '', '0');
INSERT INTO `goodstype` VALUES ('23', '兔笼', '', '22');

-- ----------------------------
-- Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL auto_increment COMMENT '主键id',
  `content` text COMMENT '内容',
  `createTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '创建时间',
  `createUserid` int(11) default NULL COMMENT '创建人id',
  `goodsid` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评论';

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('1', '很好看很听话', '2018-10-06 11:14:12', '1', '17');
INSERT INTO `message` VALUES ('2', '不错不错五星好评', '2018-10-06 11:14:12', '1', '11');

-- ----------------------------
-- Table structure for `orderdetail`
-- ----------------------------
DROP TABLE IF EXISTS `orderdetail`;
CREATE TABLE `orderdetail` (
  `id` int(11) NOT NULL auto_increment,
  `orderid` int(11) default NULL COMMENT '订单id',
  `goodsid` int(11) default NULL COMMENT '商品id',
  `goodsname` varchar(256) default NULL COMMENT '商品名称',
  `goodsnum` int(11) default NULL COMMENT '数量',
  `itemprice` decimal(10,2) default NULL COMMENT '单价',
  `totalprice` decimal(10,2) default NULL COMMENT '总价',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单明细';

-- ----------------------------
-- Records of orderdetail
-- ----------------------------
INSERT INTO `orderdetail` VALUES ('1', '1', '15', '易萌宠物活体鸟观赏虎皮鹦鹉活体手养说话小鸟', '1', null, '111.00');
INSERT INTO `orderdetail` VALUES ('2', '2', '17', '白色玄风鹦鹉两只（一公一母）', '1', null, '1230.00');
INSERT INTO `orderdetail` VALUES ('3', '2', '11', '贵货多肉', '1', null, '12.00');
INSERT INTO `orderdetail` VALUES ('4', '3', '8', '虎皮鹦鹉活体宠物鸟', '1', null, '199.88');
INSERT INTO `orderdetail` VALUES ('5', '4', '9', 'DEERA招财鱼饲料', '1', null, '50.50');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL auto_increment COMMENT '订单id',
  `buyerid` int(11) default NULL COMMENT '买家id',
  `createTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) default NULL COMMENT '订单状态：\r\n            0：未发货\r\n            1：已发货\r\n            2：已签收',
  `recipient` varchar(256) default NULL COMMENT '收件人',
  `address` varchar(1024) default NULL COMMENT '收件人地址',
  `tel` varchar(64) default NULL COMMENT '收件人手机',
  `totalPrice` decimal(10,2) default NULL COMMENT '订单总金额',
  `logistics` varchar(256) default NULL COMMENT '物流公司',
  `remark` varchar(1024) default NULL COMMENT 'varchar(256)',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '3', '2018-10-06 09:38:52', '0', 'hh', '上海市徐家汇', '12345', '103.00', null, '');
INSERT INTO `orders` VALUES ('2', '1', '2018-10-06 11:23:21', '3', '张三', '南京市新街口', '1230086', '1242.00', null, '');
INSERT INTO `orders` VALUES ('3', '1', '2018-10-06 11:25:53', '2', '测试001', '上海张江高科', '10000123', '191.88', null, '');
INSERT INTO `orders` VALUES ('4', '1', '2018-10-06 11:25:44', '0', '测试001', '江苏苏州市姑苏区', '12345', '50.50', null, '');

-- ----------------------------
-- Table structure for `shopcart`
-- ----------------------------
DROP TABLE IF EXISTS `shopcart`;
CREATE TABLE `shopcart` (
  `id` int(11) NOT NULL auto_increment,
  `createtime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '创建时间',
  `goodsid` int(11) default NULL COMMENT '商品id',
  `goodsnum` int(11) default NULL COMMENT '商品数量',
  `goodsprice` decimal(10,2) default NULL COMMENT '商品单价',
  `userid` int(11) default NULL COMMENT '用户id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车';

-- ----------------------------
-- Records of shopcart
-- ----------------------------
INSERT INTO `shopcart` VALUES ('1', '2018-10-06 10:17:28', '8', '2', null, '3');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment COMMENT '主键id',
  `username` varchar(256) default NULL COMMENT '用户名',
  `password` varchar(256) default NULL COMMENT '密码',
  `telphone` varchar(256) default NULL COMMENT '电话',
  `email` varchar(256) default NULL COMMENT '邮箱',
  `address` varchar(1024) default NULL COMMENT '地址',
  `sex` varchar(20) default NULL COMMENT '性别',
  `age` int(11) default NULL COMMENT '年龄',
  `createTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '创建时间',
  `lastLoginTime` timestamp NOT NULL default '0000-00-00 00:00:00' COMMENT '最后登录时间',
  `status` varchar(20) default NULL COMMENT '用户状态： \r\n            ON:可用，\r\n            OFF:禁用\r\n            ',
  `money` decimal(10,2) default NULL COMMENT '账户余额',
  `headimg` varchar(256) default NULL COMMENT '头像',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'test', 'test', '123456', '123456789', '南京市', '女', '23', '2018-10-06 22:35:11', '2018-09-04 22:26:28', 'ON', '18515.62', '/uploadPic/54258b9a-6072-4f4a-a8b7-09beeaec9e9a.jpg');
INSERT INTO `user` VALUES ('3', 'hh', 'hh', null, null, null, null, null, '2018-10-06 09:32:32', '2018-10-05 20:48:13', null, '19897.00', '/uploadPic/no-img_mid_.jpg');
