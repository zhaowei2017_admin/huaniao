package com.neusoft.huaniao.dao;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.Admin;

/**
 * 描述：管理员表
 */
public interface AdminDao extends BaseDao<Admin>{
	Admin login(Admin admin);
}
