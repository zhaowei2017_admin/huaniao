package com.neusoft.huaniao.model;

/**
 * 描述：管理员表
 * 
 * 创建时间：2018/08/11 17:23
 */
public class Admin{
	
	/**
	 * id
	 */
	private	Integer id;
	
	/**
	 * 登录名
	 */
	private	String uname;
	
	/**
	 * 登录密码
	 */
	private	String upassword;
	
	/**
	 * 
	 */
	private	String rolename;
	
	
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}
	public String getUname(){
		return uname;
	}
	
	public void setUname(String uname){
		this.uname=uname;
	}
	public String getUpassword(){
		return upassword;
	}
	
	public void setUpassword(String upassword){
		this.upassword=upassword;
	}
	public String getRolename(){
		return rolename;
	}
	
	public void setRolename(String rolename){
		this.rolename=rolename;
	}
	
	
}
