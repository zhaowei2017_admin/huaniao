package com.neusoft.huaniao.controller;

import org.springframework.web.bind.annotation.*;

import com.neusoft.huaniao.model.Message;
import com.neusoft.huaniao.service.MessageService;
import com.neusoft.huaniao.utils.*;

import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;
import java.util.List;  


@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    private MessageService messageService;

    /**
     * 新增
     */
    @PostMapping("/insert")
    public Result<String> insert(@RequestBody Message message){
    	message.setCreateTime(new Date());
        int res=messageService.insert(message);
        if(res==1){
            return ResultGenerator.genSuccessResult();
        }else{
        	return ResultGenerator.genFailResult(null);
        }
    }

    /**
     * 删除
     */
    @GetMapping("/delete/{ids}")
    public Result<String> delete(@PathVariable("ids") String ids){
    	if(null==ids||ids.equals("")){
    		return ResultGenerator.genFailResult(null);
    	}
        String str[]=ids.split(",");
        for(String s:str){
            messageService.deleteByPrimaryKey(Integer.valueOf(s));
        }
        return ResultGenerator.genSuccessResult();
    }
    
    /**
     * 更新
     */
    @PostMapping("/update")
    public Result<String> update(@RequestBody Message message){
    	int res=messageService.updateByPrimaryKey(message);
    	if(res==1){
    		return ResultGenerator.genSuccessResult();
    	}else{
    		return ResultGenerator.genFailResult(null);
    	}
    }
    
    /**
     * 主键查询
     */
    @GetMapping("/info/{id}")
    public Result<?> selectById(@PathVariable("id") int id){
    	Message message = messageService.selectByPrimaryKey(id);
    	if(null==message){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(message);
    	}
    }
    
    /**
     * 条件和排序的分页查询
     */
    @PostMapping("/search")
    public  HashMap<String, Object> search(
    		@RequestParam(value = "page", required = false) String page,
            @RequestParam(value = "rows", required = false) String rows,
            Message message,HttpServletResponse response){
    	//设置分页参数
    	Map<String, Object> map = new HashMap<String, Object>();
        if (page != null && rows != null) {
            PageBean pageBean = new PageBean(Integer.parseInt(page),
                    Integer.parseInt(rows));
            map.put("start", pageBean.getStart());
            map.put("size", pageBean.getPageSize());
        }
        
        
        
        //设置排序参数
        map.put("sort","id");
        
        //发送数据
        List<Message> list=messageService.select(map);
        int total=messageService.count(map);
        
        HashMap<String, Object> result=new HashMap<>();
        result.put("rows", list);
        result.put("total", total);
        return result;
    }
    
    /**
     * 根据商品id查询评论信息
     */
    @PostMapping("/selectByGoodsid")
    public Map<String,Object> selectByGoodsid(int goodsid,int page,int rows){
    	PageBean pageBean = new PageBean(page,rows);
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("start", pageBean.getStart());
        map.put("size", pageBean.getPageSize());
        map.put("goodsid", goodsid);
        
        //分页查询评论信息
        List<Message> list=messageService.select(map);
        //查询总数
        int totalSize=messageService.count(map);
        //计算总页数
        int totalPage=totalSize%rows==0?totalSize/rows:totalSize/rows+1;
        //封装返回对象
        HashMap<String, Object> result=new HashMap<>();
        result.put("data", list);
        result.put("totalSize", totalSize);
        result.put("totalPage", totalPage);
        result.put("currPage", page);
        return result;
    }
}