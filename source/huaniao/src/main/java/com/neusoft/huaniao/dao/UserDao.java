package com.neusoft.huaniao.dao;

import java.util.List;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.User;

/**
 * 描述：用户表
 * 
 * 创建时间：2018/08/11 16:52
 */
public interface UserDao extends BaseDao<User>{

	/**
	 * 登录
	 * @param user
	 * @return
	 */
	User login(User user);

	/**
	 * 查询是否重名
	 * @param username
	 * @return
	 */
	List<User> checkName(String username);

}
