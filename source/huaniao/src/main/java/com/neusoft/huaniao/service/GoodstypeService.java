package com.neusoft.huaniao.service;

import java.util.List;
import java.util.Map;

import com.neusoft.huaniao.model.Goodstype;

/**
 * 描述：商品类别表
 * 
 * 创建时间：2018/08/11 16:52
 */
public interface GoodstypeService {
	/**
	 * 插入
	 */
	int insert(Goodstype record);
    
	
	/**
	 * 根据主键删除
	 */
	int deleteByPrimaryKey(int id);
	 
	
	/**
	 * 根据主键更新
	 */
	int updateByPrimaryKey(Goodstype record);

	
	/**
	 * 根据主键查询
	 */
	Goodstype selectByPrimaryKey(int id);

	
	/**
	 * 分页、条件、排序查询
	 */
	List<Goodstype> select(Map<String,Object> map);
	
	
	/**
	 * 根据条件计算总数据量
	 */
	int count(Map<String,Object> map);

	
	/**
	 * 查询所有的父类别
	 */
	List<Goodstype> selectAllParent();
	
	
	/**
	 * 查询所有的子类别
	 */
	List<Goodstype> selectAllChildByParentId(int parentId);


	List<Goodstype> selectAllChildType();
}
