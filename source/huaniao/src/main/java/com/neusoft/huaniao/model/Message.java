package com.neusoft.huaniao.model;

import java.util.Date;

/**
 * 描述：留言板
 * 
 * 创建时间：2018/08/11 17:45
 */
public class Message{
	
	/**
	 * 主键id
	 */
	private	Integer id;
	
	/**
	 * 内容
	 */
	private	String content;
	
	/**
	 * 创建时间
	 */
	private	Date createTime;
	
	/**
	 * 创建人id
	 */
	private	Integer createUserid;
	
	/**
	 * 商品id
	 */
	private int goodsid;
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}
	public String getContent(){
		return content;
	}
	
	public void setContent(String content){
		this.content=content;
	}
	
	public Integer getCreateUserid(){
		return createUserid;
	}
	
	public void setCreateUserid(Integer createUserid){
		this.createUserid=createUserid;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getGoodsid() {
		return goodsid;
	}

	public void setGoodsid(int goodsid) {
		this.goodsid = goodsid;
	}
	
}
