package com.neusoft.huaniao.model;

import java.util.Date;
import java.util.List;

/**
 * 描述：订单表
 * 
 * 创建时间：2018/08/11 16:52
 */
public class Orders{
	
	/**
	 * 订单id
	 */
	private	Integer id;
	
	/**
	 * 优惠券id
	 */
	private Integer couponid;
	
	/**
	 * 买家id
	 */
	private	Integer buyerid;
	
	/**
	 * 创建时间
	 */
	private	Date createTime;
	
	/**
	 * 订单状态：
            0：未发货
            1：发货中
            2：已完成
	 */
	private	Integer status;
	
	/**
	 * 收件人地址
	 */
	private	String address;
	
	/**
	 * 订单总金额
	 */
	private	java.math.BigDecimal totalPrice;
	
	
	/**
	 * 收件人电话
	 */
	private String tel;
	
	/**
	 * 收件人
	 */
	private String recipient;
	
	/**
	 * 物流公司
	 */
	private String logistics;
	
	/**
	 * 买家备注
	 */
	private String remark;
	
	/**
	 * 订单明细
	 */
	private List<Orderdetail> orderdetails;
	
	
	public List<Orderdetail> getOrderdetails() {
		return orderdetails;
	}

	public void setOrderdetails(List<Orderdetail> orderdetails) {
		this.orderdetails = orderdetails;
	}

	public String getLogistics() {
		return logistics;
	}

	public void setLogistics(String logistics) {
		this.logistics = logistics;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}

	public Integer getBuyerid(){
		return buyerid;
	}
	
	public void setBuyerid(Integer buyerid){
		this.buyerid=buyerid;
	}
	
	public Integer getStatus(){
		return status;
	}
	
	public void setStatus(Integer status){
		this.status=status;
	}
	public String getAddress(){
		return address;
	}
	
	public void setAddress(String address){
		this.address=address;
	}
	public java.math.BigDecimal getTotalPrice(){
		return totalPrice;
	}
	
	public void setTotalPrice(java.math.BigDecimal totalPrice){
		this.totalPrice=totalPrice;
	}
	

	public Integer getCouponid() {
		return couponid;
	}

	public void setCouponid(Integer couponid) {
		this.couponid = couponid;
	}

	@Override
	public String toString() {
		return "Orders [id=" + id + ", buyerid=" + buyerid + ", createTime=" + createTime + ", status=" + status
				+ ", address=" + address + ", totalPrice=" + totalPrice + ", tel=" + tel + ", recipient=" + recipient
				+ ", logistics=" + logistics + ", remark=" + remark + ", orderdetails=" + orderdetails + "]";
	}
	
}
