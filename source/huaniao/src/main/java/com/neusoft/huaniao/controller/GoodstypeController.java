package com.neusoft.huaniao.controller;

import org.springframework.web.bind.annotation.*;

import com.neusoft.huaniao.model.Goodstype;
import com.neusoft.huaniao.service.GoodstypeService;
import com.neusoft.huaniao.utils.*;

import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.util.List;  


@RestController
@RequestMapping("/goodstype")
public class GoodstypeController {
    @Autowired
    private GoodstypeService goodstypeService;

    /**
     * 新增
     */
    @PostMapping("/insert")
    public Result<String> insert(@RequestBody Goodstype goodstype){
        int res=goodstypeService.insert(goodstype);
        if(res==1){
            return ResultGenerator.genSuccessResult();
        }else{
        	return ResultGenerator.genFailResult(null);
        }
    }

    /**
     * 删除
     */
    @GetMapping("/delete/{ids}")
    public Result<String> delete(@PathVariable("ids") String ids){
    	if(null==ids||ids.equals("")){
    		return ResultGenerator.genFailResult(null);
    	}
        String str[]=ids.split(",");
        for(String s:str){
            goodstypeService.deleteByPrimaryKey(Integer.valueOf(s));
        }
        return ResultGenerator.genSuccessResult();
    }
    
    /**
     * 更新
     */
    @PostMapping("/update")
    public Result<String> update(@RequestBody Goodstype goodstype){
    	int res=goodstypeService.updateByPrimaryKey(goodstype);
    	if(res==1){
    		return ResultGenerator.genSuccessResult();
    	}else{
    		return ResultGenerator.genFailResult(null);
    	}
    }
    
    /**
     * 主键查询
     */
    @GetMapping("/info/{id}")
    public Result<?> selectById(@PathVariable("id") int id){
    	Goodstype goodstype = goodstypeService.selectByPrimaryKey(id);
    	if(null==goodstype){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(goodstype);
    	}
    }
    
    /**
     * 条件和排序的分页查询
     */
    @PostMapping("/search")
    public void search(
    		@RequestParam(value = "page", required = false) String page,
            @RequestParam(value = "rows", required = false) String rows,
            Goodstype goodstype,HttpServletResponse response){
    	//设置分页参数
    	Map<String, Object> map = new HashMap<String, Object>();
        if (page != null && rows != null) {
            PageBean pageBean = new PageBean(Integer.parseInt(page),
                    Integer.parseInt(rows));
            map.put("start", pageBean.getStart());
            map.put("size", pageBean.getPageSize());
        }
        
        
        
        //设置排序参数
        map.put("sort","id");
        
        //发送数据
        List<Goodstype> list=goodstypeService.select(map);
        int total=goodstypeService.count(map);
        JSONObject result = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(list);
        result.put("rows", jsonArray);
        result.put("total", total);
        ResponseUtil.write(response,result);
    }
    
    /**
     * 查询所有的父类别
     */
    @GetMapping("/allParentType")
    public Result<?> allParentType(){
    	List<Goodstype> allParent = goodstypeService.selectAllParent();
    	if(null==allParent){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(allParent);
    	}
    }
    
    /**
     * 查询所有的子类别
     */
    @GetMapping("/allChildType")
    public Result<?> allChildType(){
    	List<Goodstype> allChild= goodstypeService.selectAllChildType();
    	if(null==allChild){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(allChild);
    	}
    }
    
    
    /**
     * 查询父类别及其子类别
     */
    @GetMapping("/allTypes")
    public List<Goodstype> getAllTypes(){
    	List<Goodstype> allParent = goodstypeService.selectAllParent();
    	List<Goodstype> allTypes=new ArrayList<>();
    	for (Goodstype goodstype : allParent) {
			List<Goodstype> childTypes = goodstypeService.selectAllChildByParentId(goodstype.getId());
    		goodstype.setChildTypes(childTypes);
    		allTypes.add(goodstype);
		}
    	return allTypes;
    }
}