package com.neusoft.huaniao.dao;

import java.util.List;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.Shopcart;

/**
 * 描述：购物车
 * 
 * 创建时间：2018/09/12 21:35
 */
public interface ShopcartDao extends BaseDao<Shopcart>{

	List<Shopcart> selectByUserid(int userid);

	/**
	 * 移除购物车数据
	 * @param shopcart
	 * @return
	 */
	int removeShopcart(Shopcart shopcart);

}
