package com.neusoft.huaniao.model;

import java.util.Date;

/**
 * 描述：商品表
 * 
 */
public class Goods{
	
	/**
	 * id
	 */
	private	Integer id;
	
	/**
	 * 商品名称
	 */
	private	String goodsname;
	
	/**
	 * 商品类别id
	 */
	private	Integer typeid;
	
	/**
	 * 创建时间
	 */
	private	Date createTime;
	
	/**
	 * 商品状态:
            0：下架
            1：上架
	 */
	private	Integer status;
	
	
	/**
	 * 价格，保留2位小数
	 */
	private	java.math.BigDecimal price;
	
	/**
	 * 商品详情
	 */
	private	String details;
	
	/**
	 * 图片url1
	 */
	private	String img1;
	
	/**
	 * 图片url2
	 */
	private	String img2;
	
	/**
	 * 图片url3
	 */
	private	String img3;
	
	/**
	 * 库存
	 */
	private int inventory;
	
	/**
	 * 销量
	 */
	private int sellNum;
	
	
	
	public int getSellNum() {
		return sellNum;
	}

	public void setSellNum(int sellNum) {
		this.sellNum = sellNum;
	}

	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}
	public String getGoodsname(){
		return goodsname;
	}
	
	public void setGoodsname(String goodsname){
		this.goodsname=goodsname;
	}
	public Integer getTypeid(){
		return typeid;
	}
	
	public void setTypeid(Integer typeid){
		this.typeid=typeid;
	}
	public Date getCreateTime(){
		return createTime;
	}
	
	public void setCreateTime(Date createTime){
		this.createTime=createTime;
	}
	public Integer getStatus(){
		return status;
	}
	
	public void setStatus(Integer status){
		this.status=status;
	}
	
	public java.math.BigDecimal getPrice(){
		return price;
	}
	
	public void setPrice(java.math.BigDecimal price){
		this.price=price;
	}
	public String getDetails(){
		return details;
	}
	
	public void setDetails(String details){
		this.details=details;
	}
	public String getImg1(){
		return img1;
	}
	
	public void setImg1(String img1){
		this.img1=img1;
	}
	public String getImg2(){
		return img2;
	}
	
	public void setImg2(String img2){
		this.img2=img2;
	}
	public String getImg3(){
		return img3;
	}
	
	public void setImg3(String img3){
		this.img3=img3;
	}

	public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}
	
}
