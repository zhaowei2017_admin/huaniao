package com.neusoft.huaniao.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neusoft.huaniao.dao.ShopcartDao;
import com.neusoft.huaniao.model.Shopcart;
import com.neusoft.huaniao.service.ShopcartService;


/**
 * 描述：购物车Service实现
 * 
 * 创建时间：2018/09/12 21:35
 */
@Service
public class ShopcartServiceImpl implements ShopcartService{
	@Autowired
	private ShopcartDao shopcartDao;
	
	/**
	 * 插入
	 */
	@Override
	public int insert(Shopcart record) {
		return shopcartDao.insert(record);
	}
    
	
	/**
	 * 根据主键删除
	 */
	@Override
	public int deleteByPrimaryKey(int id) {
		return shopcartDao.deleteByPrimaryKey(id);
	}
	 
	
	/**
	 * 根据主键更新
	 */
	@Override
	public int updateByPrimaryKey(Shopcart record) {
		return shopcartDao.updateByPrimaryKey(record);
	}

	
	/**
	 * 根据主键查询
	 */
	@Override
	public Shopcart selectByPrimaryKey(int id){
		return shopcartDao.selectByPrimaryKey(id);
	}

	
	/**
	 * 分页、条件、排序查询
	 */
	@Override
	public List<Shopcart> select(Map<String,Object> map){
		return shopcartDao.select(map);
	}
	
	
	/**
	 * 根据条件计算总数据量
	 */
	@Override
	public int count(Map<String,Object> map) {
		return shopcartDao.count(map);
	}


	@Override
	public List<Shopcart> selectByUserid(int userid) {
		return shopcartDao.selectByUserid(userid);
	}


	@Override
	public int removeShopcart(Shopcart shopcart) {
		return shopcartDao.removeShopcart(shopcart);
	}
	
}
