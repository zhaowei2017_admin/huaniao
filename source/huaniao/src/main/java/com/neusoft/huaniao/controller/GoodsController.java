package com.neusoft.huaniao.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.neusoft.huaniao.model.Goods;
import com.neusoft.huaniao.service.GoodsService;
import com.neusoft.huaniao.utils.PageBean;
import com.neusoft.huaniao.utils.Result;
import com.neusoft.huaniao.utils.ResultGenerator;  


@RestController
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    /**
     * 新增
     */
    @PostMapping("/insert")
    public Result<String> insert(@RequestBody Goods goods){
    	goods.setStatus(1);
        int res=goodsService.insert(goods);
        if(res==1){
            return ResultGenerator.genSuccessResult();
        }else{
        	return ResultGenerator.genFailResult(null);
        }
    }

    /**
     * 删除
     */
    @GetMapping("/delete/{ids}")
    public Result<String> delete(@PathVariable("ids") String ids){
    	if(null==ids||ids.equals("")){
    		return ResultGenerator.genFailResult(null);
    	}
        String str[]=ids.split(",");
        for(String s:str){
            goodsService.deleteByPrimaryKey(Integer.valueOf(s));
        }
        return ResultGenerator.genSuccessResult();
    }
    
    /**
     * 更新
     */
    @PostMapping("/update")
    public Result<String> update(@RequestBody Goods goods){
    	int res=goodsService.updateByPrimaryKey(goods);
    	if(res==1){
    		return ResultGenerator.genSuccessResult();
    	}else{
    		return ResultGenerator.genFailResult(null);
    	}
    }
    
    /**
     * 主键查询
     */
    @GetMapping("/info/{id}")
    public Result<?> selectById(@PathVariable("id") int id){
    	Goods goods = goodsService.selectByPrimaryKey(id);
    	if(null==goods){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(goods);
    	}
    }
    
    /**
     * 条件和排序的分页查询
     */
    @PostMapping("/search")
    public Map<String,Object> search(
    		@RequestParam(value = "page", required = false) String page,
            @RequestParam(value = "rows", required = false) String rows,
            Goods goods,HttpServletResponse response){
    	//设置分页参数
    	Map<String, Object> map = new HashMap<String, Object>();
        if (page != null && rows != null) {
            PageBean pageBean = new PageBean(Integer.parseInt(page),
                    Integer.parseInt(rows));
            map.put("start", pageBean.getStart());
            map.put("size", pageBean.getPageSize());
        }
        map.put("goodsname", goods.getGoodsname());
        //发送数据
        List<Goods> list=goodsService.select(map);
        int total=goodsService.count(map);
        Map<String,Object> resultMap=new HashMap<>();
        resultMap.put("rows", list);
        resultMap.put("total", total);
        return resultMap;
    }
    
    /**
     * 查询最新上架的商品
     */
    @PostMapping("/selectNewGoods")
    public List<Goods> selectNewGoods(int num){
    	List<Goods> list = goodsService.selectNewGoods(num);
    	return list;
    }
    
    /**
     * 根据商品类别计算商品数量
     */
    @GetMapping("/countByType/{typeId}")
    public Map<String,Object> countByType(@PathVariable("typeId")int typeId){
    	int count=goodsService.countByType(typeId);
    	Map<String,Object> result=new HashMap<>();
    	result.put("total", count);
    	return result;
    }
    
    
    /**
     * 根据商品类别分页查询商品列表
     */
    @PostMapping("/selectByType")
    public Map<String,Object> selectByType(@RequestParam("typeid")int typeid,
    		@RequestParam("page")String page,@RequestParam("rows")String rows){
    	
    	int totalSize=goodsService.countByType(typeid);
    	int totalPage=totalSize%Integer.parseInt(rows)==0?totalSize/Integer.parseInt(rows):totalSize/Integer.parseInt(rows)+1;
    	
    	//设置分页参数
    	Map<String, Object> map = new HashMap<String, Object>();
        if (page != null && rows != null) {
            PageBean pageBean = new PageBean(Integer.parseInt(page),
                    Integer.parseInt(rows));
            map.put("start", pageBean.getStart());
            map.put("size", pageBean.getPageSize());
        }
        map.put("typeid", typeid);
        List<Goods> list=goodsService.selectByType(map);
        
        Map<String,Object> resultMap=new HashMap<>();
        resultMap.put("data", list);
        resultMap.put("totalSize", totalSize);
        resultMap.put("totalPage", totalPage);
        resultMap.put("currPage", Integer.parseInt(page));
        return resultMap;
    }
    
    
    /**
     * 查询销量前五的商品
     */
    @GetMapping("/selectTop5")
    public List<Goods> selectTop(){
    	List<Goods> list=goodsService.selectTop();
    	return list;
    }
}