package com.neusoft.huaniao.service;

import java.util.List;
import java.util.Map;

import com.neusoft.huaniao.model.Orders;

/**
 * 描述：订单表
 * 
 * 创建时间：2018/08/11 16:52
 */
public interface OrdersService {
	/**
	 * 插入
	 */
	int insert(Orders record);
    
	
	/**
	 * 根据主键删除
	 */
	int deleteByPrimaryKey(int id);
	 
	
	/**
	 * 根据主键更新
	 */
	int updateByPrimaryKey(Orders record);

	
	/**
	 * 根据主键查询
	 */
	Orders selectByPrimaryKey(int id);

	
	/**
	 * 分页、条件、排序查询
	 */
	List<Orders> select(Map<String,Object> map);
	
	
	/**
	 * 根据条件计算总数据量
	 */
	int count(Map<String,Object> map);

	
	/**
	 * 根据userid查询订单
	 */
	List<Orders> selectByUserid(int userid);

	
	List<Orders> selectBySellerid(int sellerid);
	
}
