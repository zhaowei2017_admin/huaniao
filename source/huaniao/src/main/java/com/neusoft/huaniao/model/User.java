package com.neusoft.huaniao.model;

/**
 * 描述：用户表
 * 
 */
public class User{
	
	/**
	 * 主键id
	 */
	private	Integer id;
	
	/**
	 * 用户名
	 */
	private	String username;
	
	/**
	 * 密码
	 */
	private	String password;
	
	/**
	 * 电话
	 */
	private	String telphone;
	
	/**
	 * 邮箱
	 */
	private	String email;
	
	/**
	 * 地址
	 */
	private	String address;
	
	/**
	 * 性别
	 */
	private	String sex;
	
	/**
	 * 年龄
	 */
	private	Integer age;
	
	/**
	 * 创建时间
	 */
	private	java.sql.Timestamp createTime;
	
	/**
	 * 最后登录时间
	 */
	private	java.sql.Timestamp lastLoginTime;
	
	/**
	 * 用户状态： 
     *       ON:可用，
     *      OFF:禁用
	 */
	private	String status;
	
	/**
	 * 账户余额
	 */
	private	java.math.BigDecimal money;
	
	private String headimg;
	
	
	public String getHeadimg() {
		return headimg;
	}

	public void setHeadimg(String headimg) {
		this.headimg = headimg;
	}

	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}
	public String getUsername(){
		return username;
	}
	
	public void setUsername(String username){
		this.username=username;
	}
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password=password;
	}
	public String getTelphone(){
		return telphone;
	}
	
	public void setTelphone(String telphone){
		this.telphone=telphone;
	}
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email=email;
	}
	public String getAddress(){
		return address;
	}
	
	public void setAddress(String address){
		this.address=address;
	}
	public String getSex(){
		return sex;
	}
	
	public void setSex(String sex){
		this.sex=sex;
	}
	public Integer getAge(){
		return age;
	}
	
	public void setAge(Integer age){
		this.age=age;
	}
	public java.sql.Timestamp getCreateTime(){
		return createTime;
	}
	
	public void setCreateTime(java.sql.Timestamp createTime){
		this.createTime=createTime;
	}
	public java.sql.Timestamp getLastLoginTime(){
		return lastLoginTime;
	}
	
	public void setLastLoginTime(java.sql.Timestamp lastLoginTime){
		this.lastLoginTime=lastLoginTime;
	}
	public String getStatus(){
		return status;
	}
	
	public void setStatus(String status){
		this.status=status;
	}
	public java.math.BigDecimal getMoney(){
		return money;
	}
	
	public void setMoney(java.math.BigDecimal money){
		this.money=money;
	}
	
	
}
