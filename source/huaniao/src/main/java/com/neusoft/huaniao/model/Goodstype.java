package com.neusoft.huaniao.model;

import java.util.List;

/**
 * 描述：商品类别表
 * 
 * 创建时间：2018/08/11 16:52
 */
public class Goodstype{
	
	/**
	 * 主键id
	 */
	private	Integer id;
	
	/**
	 * 类别名称
	 */
	private	String typename;
	
	/**
	 * 类别代码
	 */
	private	String typecode;
	
	/**
	 * 父类别id
	 */
	private	Integer parentid;
	
	/**
	 * 子类别
	 */
	private List<Goodstype> childTypes;
	
	
	
	public List<Goodstype> getChildTypes() {
		return childTypes;
	}

	public void setChildTypes(List<Goodstype> childTypes) {
		this.childTypes = childTypes;
	}

	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}
	public String getTypename(){
		return typename;
	}
	
	public void setTypename(String typename){
		this.typename=typename;
	}
	public String getTypecode(){
		return typecode;
	}
	
	public void setTypecode(String typecode){
		this.typecode=typecode;
	}
	public Integer getParentid(){
		return parentid;
	}
	
	public void setParentid(Integer parentid){
		this.parentid=parentid;
	}
	
	
}
