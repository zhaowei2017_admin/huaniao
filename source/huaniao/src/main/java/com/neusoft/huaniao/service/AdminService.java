package com.neusoft.huaniao.service;

import java.util.List;
import java.util.Map;

import com.neusoft.huaniao.model.Admin;

/**
 * 描述：管理员表
 * 
 * 创建时间：2018/08/11 17:23
 */
public interface AdminService {
	/**
	 * 插入
	 */
	int insert(Admin record);
    
	
	/**
	 * 根据主键删除
	 */
	int deleteByPrimaryKey(int id);
	 
	
	/**
	 * 根据主键更新
	 */
	int updateByPrimaryKey(Admin record);

	
	/**
	 * 根据主键查询
	 */
	Admin selectByPrimaryKey(int id);

	
	/**
	 * 分页、条件、排序查询
	 */
	List<Admin> select(Map<String,Object> map);
	
	
	/**
	 * 根据条件计算总数据量
	 */
	int count(Map<String,Object> map);


	Admin login(Admin admin);
	
}
