package com.neusoft.huaniao.model;

/**
 * 描述：购物车
 * 
 * 创建时间：2018/09/12 21:35
 */
public class Shopcart{
	
	/**
	 * 主键id
	 */
	private	Integer id;
	
	/**
	 * 创建时间
	 */
	private	java.util.Date createtime;
	
	/**
	 * 商品id
	 */
	private	Integer goodsid;
	
	/**
	 * 商品数量
	 */
	private	Integer goodsnum;
	
	/**
	 * 商品单价
	 */
	private	java.math.BigDecimal goodsprice;
	
	/**
	 * 用户id
	 */
	private	Integer userid;
	
	
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}
	public java.util.Date getCreatetime(){
		return createtime;
	}
	
	public void setCreatetime(java.util.Date createtime){
		this.createtime=createtime;
	}
	public Integer getGoodsid(){
		return goodsid;
	}
	
	public void setGoodsid(Integer goodsid){
		this.goodsid=goodsid;
	}
	public Integer getGoodsnum(){
		return goodsnum;
	}
	
	public void setGoodsnum(Integer goodsnum){
		this.goodsnum=goodsnum;
	}
	public java.math.BigDecimal getGoodsprice(){
		return goodsprice;
	}
	
	public void setGoodsprice(java.math.BigDecimal goodsprice){
		this.goodsprice=goodsprice;
	}
	public Integer getUserid(){
		return userid;
	}
	
	public void setUserid(Integer userid){
		this.userid=userid;
	}
	
	
}
