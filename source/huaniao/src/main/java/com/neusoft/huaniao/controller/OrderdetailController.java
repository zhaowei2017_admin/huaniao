package com.neusoft.huaniao.controller;

import org.springframework.web.bind.annotation.*;

import com.neusoft.huaniao.model.Orderdetail;
import com.neusoft.huaniao.service.OrderdetailService;
import com.neusoft.huaniao.utils.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;  


@RestController
@RequestMapping("/orderdetail")
public class OrderdetailController {
    @Autowired
    private OrderdetailService orderdetailService;

    /**
     * 新增
     */
    @PostMapping("/insert")
    public Result<String> insert(@RequestBody Orderdetail orderdetail){
        int res=orderdetailService.insert(orderdetail);
        if(res==1){
            return ResultGenerator.genSuccessResult();
        }else{
        	return ResultGenerator.genFailResult(null);
        }
    }

    /**
     * 删除
     */
    @GetMapping("/delete/{ids}")
    public Result<String> delete(@PathVariable("ids") String ids){
    	if(null==ids||ids.equals("")){
    		return ResultGenerator.genFailResult(null);
    	}
        String str[]=ids.split(",");
        for(String s:str){
            orderdetailService.deleteByPrimaryKey(Integer.valueOf(s));
        }
        return ResultGenerator.genSuccessResult();
    }
    
    /**
     * 更新
     */
    @PostMapping("/update")
    public Result<String> update(@RequestBody Orderdetail orderdetail){
    	int res=orderdetailService.updateByPrimaryKey(orderdetail);
    	if(res==1){
    		return ResultGenerator.genSuccessResult();
    	}else{
    		return ResultGenerator.genFailResult(null);
    	}
    }
    
    /**
     * 主键查询
     */
    @GetMapping("/info/{id}")
    public Result<?> selectById(@PathVariable("id") int id){
    	Orderdetail orderdetail = orderdetailService.selectByPrimaryKey(id);
    	if(null==orderdetail){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(orderdetail);
    	}
    }
    
    /**
     * 根据订单号查询订单明细
     */
    @GetMapping("/selectByOrderid/{orderid}")
    public List<Orderdetail> selectByOrderid(@PathVariable("orderid")int orderid){
    	List<Orderdetail> list=orderdetailService.selectByOrderid(orderid);
    	return list;
    }
}