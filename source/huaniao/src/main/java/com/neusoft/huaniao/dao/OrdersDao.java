package com.neusoft.huaniao.dao;

import java.util.List;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.Orders;

/**
 * 描述：订单表
 * 
 * 创建时间：2018/08/11 16:52
 */
public interface OrdersDao extends BaseDao<Orders>{

	List<Orders> selectByUserid(int userid);

	List<Orders> selectBySellerid(int sellerid);

}
