package com.neusoft.huaniao.service;

import java.util.List;
import java.util.Map;

import com.neusoft.huaniao.model.User;

/**
 * 描述：用户表
 * 
 * 创建时间：2018/08/11 16:52
 */
public interface UserService {
	/**
	 * 插入
	 */
	int insert(User record);
    
	
	/**
	 * 根据主键删除
	 */
	int deleteByPrimaryKey(int id);
	 
	
	/**
	 * 根据主键更新
	 */
	int updateByPrimaryKey(User record);

	
	/**
	 * 根据主键查询
	 */
	User selectByPrimaryKey(int id);

	
	/**
	 * 分页、条件、排序查询
	 */
	List<User> select(Map<String,Object> map);
	
	
	/**
	 * 根据条件计算总数据量
	 */
	int count(Map<String,Object> map);


	User login(User user);

	/**
	 * 查询是否重名
	 * @param username
	 * @return
	 */
	List<User> checkName(String username);
	
}
