package com.neusoft.huaniao.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neusoft.huaniao.dao.OrdersDao;
import com.neusoft.huaniao.model.Orders;
import com.neusoft.huaniao.service.OrdersService;


/**
 * 描述：订单表Service实现
 * 
 * 创建时间：2018/08/11 16:52
 */
@Service
public class OrdersServiceImpl implements OrdersService{
	@Autowired
	private OrdersDao ordersDao;
	
	/**
	 * 插入
	 */
	@Override
	public int insert(Orders record) {
		return ordersDao.insert(record);
	}
    
	
	/**
	 * 根据主键删除
	 */
	@Override
	public int deleteByPrimaryKey(int id) {
		return ordersDao.deleteByPrimaryKey(id);
	}
	 
	
	/**
	 * 根据主键更新
	 */
	@Override
	public int updateByPrimaryKey(Orders record) {
		return ordersDao.updateByPrimaryKey(record);
	}

	
	/**
	 * 根据主键查询
	 */
	@Override
	public Orders selectByPrimaryKey(int id){
		return ordersDao.selectByPrimaryKey(id);
	}

	
	/**
	 * 分页、条件、排序查询
	 */
	@Override
	public List<Orders> select(Map<String,Object> map){
		return ordersDao.select(map);
	}
	
	
	/**
	 * 根据条件计算总数据量
	 */
	@Override
	public int count(Map<String,Object> map) {
		return ordersDao.count(map);
	}


	@Override
	public List<Orders> selectByUserid(int userid) {
		return ordersDao.selectByUserid(userid);
	}


	@Override
	public List<Orders> selectBySellerid(int sellerid) {
		return ordersDao.selectBySellerid(sellerid);
	}
	
}
