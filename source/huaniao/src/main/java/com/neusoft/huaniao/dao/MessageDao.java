package com.neusoft.huaniao.dao;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.Message;

/**
 * 描述：留言板
 * 
 * 创建时间：2018/08/11 17:45
 */
public interface MessageDao extends BaseDao<Message>{

}
