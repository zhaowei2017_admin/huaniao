package com.neusoft.huaniao.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.neusoft.huaniao.model.Shopcart;
import com.neusoft.huaniao.service.ShopcartService;
import com.neusoft.huaniao.utils.Result;
import com.neusoft.huaniao.utils.ResultGenerator;  


@RestController
@RequestMapping("/shopcart")
public class ShopcartController {
    @Autowired
    private ShopcartService shopcartService;

    /**
     * 新增
     */
    @PostMapping("/insert")
    public Result<String> insert(@RequestBody Shopcart shopcart){
    	//先检查购物车是否有相同的商品
    	//如果有则更新，没有则新增
    	List<Shopcart> list = shopcartService.selectByUserid(shopcart.getUserid());
    	for (Shopcart item : list) {
			if(item.getGoodsid()==shopcart.getGoodsid()){
				item.setGoodsnum(item.getGoodsnum()+shopcart.getGoodsnum());
				shopcartService.updateByPrimaryKey(item);
				return ResultGenerator.genSuccessResult();
			}
		}
    	
    	shopcart.setCreatetime(new Date());
        int res=shopcartService.insert(shopcart);
        if(res==1){
            return ResultGenerator.genSuccessResult();
        }else{
        	return ResultGenerator.genFailResult(null);
        }
    }

    /**
     * 删除
     */
    @GetMapping("/delete/{ids}")
    public Result<String> delete(@PathVariable("ids") String ids){
    	if(null==ids||ids.equals("")){
    		return ResultGenerator.genFailResult(null);
    	}
        String str[]=ids.split(",");
        for(String s:str){
            shopcartService.deleteByPrimaryKey(Integer.valueOf(s));
        }
        return ResultGenerator.genSuccessResult();
    }
    
    /**
     * 更新
     */
    @PostMapping("/update")
    public Result<String> update(@RequestBody Shopcart shopcart){
    	int res=shopcartService.updateByPrimaryKey(shopcart);
    	if(res==1){
    		return ResultGenerator.genSuccessResult();
    	}else{
    		return ResultGenerator.genFailResult(null);
    	}
    }
    
    /**
     * 主键查询
     */
    @GetMapping("/info/{id}")
    public Result<?> selectById(@PathVariable("id") int id){
    	Shopcart shopcart = shopcartService.selectByPrimaryKey(id);
    	if(null==shopcart){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(shopcart);
    	}
    }
    
    /**
     * 根据用户id查询购物车数据
     */
    @GetMapping("/selectByUserid/{userid}")
    public List<Shopcart> selectByUserid(@PathVariable("userid")int userid){
    	List<Shopcart> list=shopcartService.selectByUserid(userid);
    	return list;
    }
    
    /**
     * 根据用户id和商品id移除购物车
     */
    @PostMapping("/removeShopcart")
    public Result<?> removeShopcart(Shopcart shopcart){
    	int rs=shopcartService.removeShopcart(shopcart);
    	if(rs>0){//成功
    		return ResultGenerator.genSuccessResult();
    	}else{
    		return ResultGenerator.genFailResult("移除购物车失败");
    	}
    }
}