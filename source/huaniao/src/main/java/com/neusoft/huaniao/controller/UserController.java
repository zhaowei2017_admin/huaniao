package com.neusoft.huaniao.controller;

import org.springframework.web.bind.annotation.*;
import com.neusoft.huaniao.dao.CouponDao;
import com.neusoft.huaniao.model.Coupon;
import com.neusoft.huaniao.model.User;
import com.neusoft.huaniao.service.UserService;
import com.neusoft.huaniao.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.math.BigDecimal;
import java.util.HashMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.util.List;  


@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private CouponDao couponDao;
    
    /**
     * 登录
     */
    @PostMapping("/login")
    public Result<?> login(User user){
    	User u=userService.login(user);
    	if(u!=null){
    		return ResultGenerator.genSuccessResult(u);
    	}else{
    		return ResultGenerator.genFailResult("登录失败");
    	}
    }
    

    /**
     * 注册
     */
    @PostMapping("/insert")
    public Result<String> insert(@RequestBody User user){
    	user.setMoney(new BigDecimal(20000));
    	user.setHeadimg("/uploadPic/no-img_mid_.jpg");//设置默认头像
        int res=userService.insert(user);
        if(res==1){
        	//新户赠送一张8元优惠券
        	Coupon coupon=new Coupon();
        	coupon.setUserid(user.getId());
        	coupon.setCouponValue(new BigDecimal(8));
        	coupon.setCouponStatus(0);
        	couponDao.insert(coupon);
            return ResultGenerator.genSuccessResult();
        }else{
        	return ResultGenerator.genFailResult(null);
        }
    }

    /**
     * 删除
     */
    @GetMapping("/delete/{ids}")
    public Result<String> delete(@PathVariable("ids") String ids){
    	if(null==ids||ids.equals("")){
    		return ResultGenerator.genFailResult(null);
    	}
        String str[]=ids.split(",");
        for(String s:str){
            userService.deleteByPrimaryKey(Integer.valueOf(s));
        }
        return ResultGenerator.genSuccessResult();
    }
    
    /**
     * 更新
     */
    @PostMapping("/update")
    public Result<String> update(@RequestBody User user){
    	int res=userService.updateByPrimaryKey(user);
    	if(res==1){
    		return ResultGenerator.genSuccessResult();
    	}else{
    		return ResultGenerator.genFailResult(null);
    	}
    }
    
    /**
     * 主键查询
     */
    @GetMapping("/info/{id}")
    public Result<?> selectById(@PathVariable("id") int id){
    	User user = userService.selectByPrimaryKey(id);
    	if(null==user){
    		return ResultGenerator.genFailResult(null);
    	}else{
    		return ResultGenerator.genSuccessResult(user);
    	}
    }
    
    /**
     * 条件和排序的分页查询
     */
    @PostMapping("/search")
    public void search(
    		@RequestParam(value = "page", required = false) String page,
            @RequestParam(value = "rows", required = false) String rows,
            User user,HttpServletResponse response){
    	//设置分页参数
    	Map<String, Object> map = new HashMap<String, Object>();
        if (page != null && rows != null) {
            PageBean pageBean = new PageBean(Integer.parseInt(page),
                    Integer.parseInt(rows));
            map.put("start", pageBean.getStart());
            map.put("size", pageBean.getPageSize());
        }
        
        //设置查询参数
        map.put("searchParam",user.getUsername());
        List<User> list=userService.select(map);
        int total=userService.count(map);
        JSONObject result = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(list);
        result.put("rows", jsonArray);
        result.put("total", total);
        ResponseUtil.write(response,result);
    }
    
    /**
     * 重复用户名查询
     */
    @PostMapping("/checkName")
    public Map<String,Object> checkName(@RequestParam("username")String username){
    	Map<String,Object> rs=new HashMap<>();
    	List<User> list= userService.checkName(username);
    	if(list.size()>0){
    		rs.put("code", 400);
    		return rs;
    	}else{
    		rs.put("code", 200);
    		return rs;
    	}
    }
    
    /**
     * 查询用户的优惠券
     */
    @GetMapping("/selectCoupons/{userid}/{status}")
    public List<Coupon> selectCoupons(@PathVariable("userid")int userid,@PathVariable("status")int status){
    	return couponDao.selectByUserid(userid,status);
    }
    
    /**
     * 根据优惠券id查询优惠券
     */
    @GetMapping("/selectCouponById/{couponid}")
    public Coupon selectCouponById(@PathVariable("couponid")int couponid){
    	return couponDao.selectByPrimaryKey(couponid);
    }
}