package com.neusoft.huaniao.service;

import java.util.List;
import java.util.Map;

import com.neusoft.huaniao.model.Orderdetail;

/**
 * 描述：订单明细
 * 
 * 创建时间：2018/09/09 11:24
 */
public interface OrderdetailService {
	/**
	 * 插入
	 */
	int insert(Orderdetail record);
    
	
	/**
	 * 根据主键删除
	 */
	int deleteByPrimaryKey(int id);
	 
	
	/**
	 * 根据主键更新
	 */
	int updateByPrimaryKey(Orderdetail record);

	
	/**
	 * 根据主键查询
	 */
	Orderdetail selectByPrimaryKey(int id);

	
	/**
	 * 分页、条件、排序查询
	 */
	List<Orderdetail> select(Map<String,Object> map);
	
	
	/**
	 * 根据条件计算总数据量
	 */
	int count(Map<String,Object> map);

	/**
     * 根据订单号查询订单明细
     */
	List<Orderdetail> selectByOrderid(int orderid);
	
}
