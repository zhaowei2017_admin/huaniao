package com.neusoft.huaniao.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neusoft.huaniao.dao.GoodstypeDao;
import com.neusoft.huaniao.model.Goodstype;
import com.neusoft.huaniao.service.GoodstypeService;


/**
 * 描述：商品类别表Service实现
 * 
 * 创建时间：2018/08/11 16:52
 */
@Service
public class GoodstypeServiceImpl implements GoodstypeService{
	@Autowired
	private GoodstypeDao goodstypeDao;
	
	/**
	 * 插入
	 */
	@Override
	public int insert(Goodstype record) {
		return goodstypeDao.insert(record);
	}
    
	
	/**
	 * 根据主键删除
	 */
	@Override
	public int deleteByPrimaryKey(int id) {
		return goodstypeDao.deleteByPrimaryKey(id);
	}
	 
	
	/**
	 * 根据主键更新
	 */
	@Override
	public int updateByPrimaryKey(Goodstype record) {
		return goodstypeDao.updateByPrimaryKey(record);
	}

	
	/**
	 * 根据主键查询
	 */
	@Override
	public Goodstype selectByPrimaryKey(int id){
		return goodstypeDao.selectByPrimaryKey(id);
	}

	
	/**
	 * 分页、条件、排序查询
	 */
	@Override
	public List<Goodstype> select(Map<String,Object> map){
		return goodstypeDao.select(map);
	}
	
	
	/**
	 * 根据条件计算总数据量
	 */
	@Override
	public int count(Map<String,Object> map) {
		return goodstypeDao.count(map);
	}


	@Override
	public List<Goodstype> selectAllParent() {
		return goodstypeDao.selectAllParent();
	}


	@Override
	public List<Goodstype> selectAllChildByParentId(int parentId) {
		return goodstypeDao.selectAllChildByParentId(parentId);
	}


	@Override
	public List<Goodstype> selectAllChildType() {
		return goodstypeDao.selectAllChildType();
	}


}
