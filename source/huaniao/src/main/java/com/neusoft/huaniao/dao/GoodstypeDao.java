package com.neusoft.huaniao.dao;

import java.util.List;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.Goodstype;

/**
 * 描述：商品类别表
 * 
 * 创建时间：2018/08/11 16:52
 */
public interface GoodstypeDao extends BaseDao<Goodstype>{
	/**
	 * 查询所有的父类别
	 * @return
	 */
	public List<Goodstype> selectAllParent();


	/**
	 * 根据父类id查询所有子类别
	 * @param parentId
	 * @return
	 */
	public List<Goodstype> selectAllChildByParentId(int parentId);


	public List<Goodstype> selectAllChildType();
}
