package com.neusoft.huaniao.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neusoft.huaniao.dao.OrderdetailDao;
import com.neusoft.huaniao.model.Orderdetail;
import com.neusoft.huaniao.service.OrderdetailService;


/**
 * 描述：订单明细Service实现
 * 
 * 创建时间：2018/09/09 11:24
 */
@Service
public class OrderdetailServiceImpl implements OrderdetailService{
	@Autowired
	private OrderdetailDao orderdetailDao;
	
	/**
	 * 插入
	 */
	@Override
	public int insert(Orderdetail record) {
		return orderdetailDao.insert(record);
	}
    
	
	/**
	 * 根据主键删除
	 */
	@Override
	public int deleteByPrimaryKey(int id) {
		return orderdetailDao.deleteByPrimaryKey(id);
	}
	 
	
	/**
	 * 根据主键更新
	 */
	@Override
	public int updateByPrimaryKey(Orderdetail record) {
		return orderdetailDao.updateByPrimaryKey(record);
	}

	
	/**
	 * 根据主键查询
	 */
	@Override
	public Orderdetail selectByPrimaryKey(int id){
		return orderdetailDao.selectByPrimaryKey(id);
	}

	
	/**
	 * 分页、条件、排序查询
	 */
	@Override
	public List<Orderdetail> select(Map<String,Object> map){
		return orderdetailDao.select(map);
	}
	
	
	/**
	 * 根据条件计算总数据量
	 */
	@Override
	public int count(Map<String,Object> map) {
		return orderdetailDao.count(map);
	}

	/**
     * 根据订单号查询订单明细
     */
	@Override
	public List<Orderdetail> selectByOrderid(int orderid) {
		return orderdetailDao.selectByOrderid(orderid);
	}
	
}
