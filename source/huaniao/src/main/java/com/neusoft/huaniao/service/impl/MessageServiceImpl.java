package com.neusoft.huaniao.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neusoft.huaniao.dao.MessageDao;
import com.neusoft.huaniao.model.Message;
import com.neusoft.huaniao.service.MessageService;


/**
 * 描述：留言板Service实现
 * 
 * 创建时间：2018/08/11 17:45
 */
@Service
public class MessageServiceImpl implements MessageService{
	@Autowired
	private MessageDao messageDao;
	
	/**
	 * 插入
	 */
	@Override
	public int insert(Message record) {
		return messageDao.insert(record);
	}
    
	
	/**
	 * 根据主键删除
	 */
	@Override
	public int deleteByPrimaryKey(int id) {
		return messageDao.deleteByPrimaryKey(id);
	}
	 
	
	/**
	 * 根据主键更新
	 */
	@Override
	public int updateByPrimaryKey(Message record) {
		return messageDao.updateByPrimaryKey(record);
	}

	
	/**
	 * 根据主键查询
	 */
	@Override
	public Message selectByPrimaryKey(int id){
		return messageDao.selectByPrimaryKey(id);
	}

	
	/**
	 * 分页、条件、排序查询
	 */
	@Override
	public List<Message> select(Map<String,Object> map){
		return messageDao.select(map);
	}
	
	
	/**
	 * 根据条件计算总数据量
	 */
	@Override
	public int count(Map<String,Object> map) {
		return messageDao.count(map);
	}
	
}
