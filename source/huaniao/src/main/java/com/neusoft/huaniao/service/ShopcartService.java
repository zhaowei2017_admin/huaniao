package com.neusoft.huaniao.service;

import java.util.List;
import java.util.Map;

import com.neusoft.huaniao.model.Shopcart;

/**
 * 描述：购物车
 * 
 * 创建时间：2018/09/12 21:35
 */
public interface ShopcartService {
	/**
	 * 插入
	 */
	int insert(Shopcart record);
    
	
	/**
	 * 根据主键删除
	 */
	int deleteByPrimaryKey(int id);
	 
	
	/**
	 * 根据主键更新
	 */
	int updateByPrimaryKey(Shopcart record);

	
	/**
	 * 根据主键查询
	 */
	Shopcart selectByPrimaryKey(int id);

	
	/**
	 * 分页、条件、排序查询
	 */
	List<Shopcart> select(Map<String,Object> map);
	
	
	/**
	 * 根据条件计算总数据量
	 */
	int count(Map<String,Object> map);


	List<Shopcart> selectByUserid(int userid);

	/**
	 * 移除购物车数据
	 * @param shopcart
	 * @return
	 */
	int removeShopcart(Shopcart shopcart);
	
}
