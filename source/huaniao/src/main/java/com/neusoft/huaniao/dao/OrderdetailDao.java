package com.neusoft.huaniao.dao;

import java.util.List;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.Orderdetail;

/**
 * 描述：订单明细
 * 
 * 创建时间：2018/09/09 11:24
 */
public interface OrderdetailDao extends BaseDao<Orderdetail>{
	/**
     * 根据订单号查询订单明细
     */
	List<Orderdetail> selectByOrderid(int orderid);

}
