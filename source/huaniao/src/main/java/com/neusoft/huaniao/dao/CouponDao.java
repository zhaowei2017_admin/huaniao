package com.neusoft.huaniao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.neusoft.huaniao.model.Coupon;

/**
 * 折扣券dao
 *
 */
public interface CouponDao extends BaseDao<Coupon> {
	/**
	 * 根据用户id查询券
	 */
	List<Coupon> selectByUserid(@Param("userid")int userid,@Param("status")int status);
}
