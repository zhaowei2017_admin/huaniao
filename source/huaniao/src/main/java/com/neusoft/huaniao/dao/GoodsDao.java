package com.neusoft.huaniao.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

import com.neusoft.huaniao.dao.BaseDao;
import com.neusoft.huaniao.model.Goods;

/**
 * 描述：商品表
 * 
 * 创建时间：2018/08/11 17:54
 */
public interface GoodsDao extends BaseDao<Goods>{

	List<Goods> selectNewGoods(@Param("num")int num);
	
	/**
	 * 根据商品类别计算商品数量
	 */
	int countByType(int typeId);

	/**
	 * 根据商品类别分页查询商品信息
	 */
	List<Goods> selectByType(Map<String, Object> map);

	

	/**
	 * 查询销量前5
	 */
	List<Goods> selectTop();

}
