package com.neusoft.huaniao.model;

import java.math.BigDecimal;

/**
 * 抵扣券
 *
 */
public class Coupon {
	/**
	 * 主键id
	 */
	private int id;
	
	/**
	 * 用户id
	 */
	private int userid;
	
	/**
	 * 抵扣券面额
	 */
	private BigDecimal couponValue;
	
	/**
	 * 状态：   0：未使用     1：已使用
	 */
	private int couponStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public BigDecimal getCouponValue() {
		return couponValue;
	}

	public void setCouponValue(BigDecimal couponValue) {
		this.couponValue = couponValue;
	}

	public int getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(int couponStatus) {
		this.couponStatus = couponStatus;
	}
	
	
}
