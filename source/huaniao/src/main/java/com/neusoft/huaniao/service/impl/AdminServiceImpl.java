package com.neusoft.huaniao.service.impl;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neusoft.huaniao.dao.AdminDao;
import com.neusoft.huaniao.model.Admin;
import com.neusoft.huaniao.service.AdminService;


/**
 * 描述：管理员表Service实现
 * 
 * 创建时间：2018/08/11 17:23
 */
@Service
public class AdminServiceImpl implements AdminService{
	@Autowired
	private AdminDao adminDao;
	
	/**
	 * 插入
	 */
	@Override
	public int insert(Admin record) {
		return adminDao.insert(record);
	}
    
	
	/**
	 * 根据主键删除
	 */
	@Override
	public int deleteByPrimaryKey(int id) {
		return adminDao.deleteByPrimaryKey(id);
	}
	 
	
	/**
	 * 根据主键更新
	 */
	@Override
	public int updateByPrimaryKey(Admin record) {
		return adminDao.updateByPrimaryKey(record);
	}

	
	/**
	 * 根据主键查询
	 */
	@Override
	public Admin selectByPrimaryKey(int id){
		return adminDao.selectByPrimaryKey(id);
	}

	
	/**
	 * 分页、条件、排序查询
	 */
	@Override
	public List<Admin> select(Map<String,Object> map){
		return adminDao.select(map);
	}
	
	
	/**
	 * 根据条件计算总数据量
	 */
	@Override
	public int count(Map<String,Object> map) {
		return adminDao.count(map);
	}


	@Override
	public Admin login(Admin admin) {
		return adminDao.login(admin);
	}
}
