package com.neusoft.huaniao.service;

import java.util.List;
import java.util.Map;

import com.neusoft.huaniao.model.Message;

/**
 * 描述：留言板
 * 
 * 创建时间：2018/08/11 17:45
 */
public interface MessageService {
	/**
	 * 插入
	 */
	int insert(Message record);
    
	
	/**
	 * 根据主键删除
	 */
	int deleteByPrimaryKey(int id);
	 
	
	/**
	 * 根据主键更新
	 */
	int updateByPrimaryKey(Message record);

	
	/**
	 * 根据主键查询
	 */
	Message selectByPrimaryKey(int id);

	
	/**
	 * 分页、条件、排序查询
	 */
	List<Message> select(Map<String,Object> map);
	
	
	/**
	 * 根据条件计算总数据量
	 */
	int count(Map<String,Object> map);
	
}
