package com.neusoft.huaniao.model;

/**
 * 描述：订单明细
 * 
 * 创建时间：2018/09/09 11:24
 */
public class Orderdetail{
	
	/**
	 * 
	 */
	private	Integer id;
	
	/**
	 * 订单id
	 */
	private	Integer orderid;
	
	/**
	 * 商品id
	 */
	private	Integer goodsid;
	
	/**
	 * 商品名称
	 */
	private	String goodsname;
	
	/**
	 * 数量
	 */
	private	Integer goodsnum;
	
	/**
	 * 单价
	 */
	private	java.math.BigDecimal itemprice;
	
	/**
	 * 总价
	 */
	private	java.math.BigDecimal totalprice;
	
	
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getOrderid(){
		return orderid;
	}
	
	public void setOrderid(Integer orderid){
		this.orderid=orderid;
	}
	public Integer getGoodsid(){
		return goodsid;
	}
	
	public void setGoodsid(Integer goodsid){
		this.goodsid=goodsid;
	}
	public String getGoodsname(){
		return goodsname;
	}
	
	public void setGoodsname(String goodsname){
		this.goodsname=goodsname;
	}
	public Integer getGoodsnum(){
		return goodsnum;
	}
	
	public void setGoodsnum(Integer goodsnum){
		this.goodsnum=goodsnum;
	}
	public java.math.BigDecimal getItemprice(){
		return itemprice;
	}
	
	public void setItemprice(java.math.BigDecimal itemprice){
		this.itemprice=itemprice;
	}
	public java.math.BigDecimal getTotalprice(){
		return totalprice;
	}
	
	public void setTotalprice(java.math.BigDecimal totalprice){
		this.totalprice=totalprice;
	}
	
	
}
