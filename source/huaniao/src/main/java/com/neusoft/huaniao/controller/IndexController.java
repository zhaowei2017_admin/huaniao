package com.neusoft.huaniao.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	@RequestMapping("/")
	public void portal(HttpServletRequest request,HttpServletResponse response){
		try {
			response.sendRedirect("/portal/home/index.html");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
